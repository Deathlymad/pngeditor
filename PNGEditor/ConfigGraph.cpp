#include "ConfigGraph.h"
#include "Util.h"

#include <boost/filesystem.hpp>


ImageFile::ImageFile(std::string path, DirFile* parentData) : png(path)
{
	this->path = path;
	this->parentData = parentData;
	png.load();
}

File::FileType ImageFile::getType() { return PNG; }

void ImageFile::read()
{
	nlohmann::json json;
	for (uint32_t i = 0; i < 10; i++)
		json[png.getKey(i)] = png.getValue(i);
	parentData->addJsonObj(json, getFileNameFromPath(path));
}

void ImageFile::write(std::string prefix, uint32_t offset)
{
	png.setValue("Title", parentData->getTitle(path).c_str());
	png.setValue("Author", parentData->getAuthor(path).c_str());
	png.setValue("Description", parentData->getDescription(path).c_str());
	png.setValue("Copyright", parentData->getCopyright(path).c_str());
	png.setValue("Creation Time", parentData->getCreationTime(path).c_str());
	png.setValue("Software", parentData->getSoftware(path).c_str());
	png.setValue("Disclaimer", parentData->getDisclaimer(path).c_str());
	png.setValue("Warning", parentData->getWarning(path).c_str());
	png.setValue("Source", parentData->getSource(path).c_str());
	png.setValue("Comment", parentData->getComment(path).c_str());


	std::string p = path;
	p.replace(p.begin(), p.begin() + offset, prefix.begin(), prefix.end());
	png.write(p);
}


DirFile::DirFile(std::string path, DirFile* parentData)
{
	this->path = path;
	this->parentData = parentData;
	read();
}

std::string DirFile::getTitle(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !(data.at(name).at("Title").is_null()) && !(data.at(name).at("Title").get<std::string>().empty()))
		return data.at(name).at("Title").get<std::string>();
	else if (!(data["Title"].is_null()))
		return data.at("Title").get<std::string>();
	else
		return parentData->getTitle(this->path);
}

std::string DirFile::getAuthor(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Author").is_null() && !(data.at(name).at("Author").get<std::string>().empty()))
		return data.at(name).at("Author").get<std::string>();
	else if (!(data["Author"].is_null()))
		return data.at("Author").get<std::string>();
	else
		return parentData->getAuthor(this->path);
}

std::string DirFile::getDescription(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Description").is_null() && !(data.at(name).at("Description").get<std::string>().empty()))
		return data.at(name).at("Description").get<std::string>();
	else if (!(data["Description"].is_null()))
		return data.at("Description").get<std::string>();
	else
		return parentData->getDescription(this->path);
}

std::string DirFile::getCopyright(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Copyright").is_null() && !(data.at(name).at("Copyright").get<std::string>().empty()))
		return data.at(name).at("Copyright").get<std::string>();
	else if (!(data["Copyright"].is_null()))
		return data.at("Copyright").get<std::string>();
	else
		return parentData->getCopyright(this->path);
}

std::string DirFile::getCreationTime(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Creation Time").is_null() && !(data.at(name).at("Creation Time").get<std::string>().empty()))
		return data.at(name).at("Creation Time").get<std::string>();
	else if (!(data["Creation Time"].is_null()))
		return data.at("Creation Time").get<std::string>();
	else
		return parentData->getCreationTime(this->path);
}

std::string DirFile::getSoftware(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Software").is_null() && !(data.at(name).at("Software").get<std::string>().empty()))
		return data.at(name).at("Software").get<std::string>();
	else if (!(data["Software"].is_null()))
		return data.at("Software").get<std::string>();
	else
		return parentData->getSoftware(this->path);
}

std::string DirFile::getDisclaimer(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Disclaimer").is_null() && !(data.at(name).at("Disclaimer").get<std::string>().empty()))
		return data.at(name).at("Disclaimer").get<std::string>();
	else if (!(data["Disclaimer"].is_null()))
		return data.at("Disclaimer").get<std::string>();
	else
		return parentData->getDisclaimer(this->path);
}

std::string DirFile::getWarning(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Warning").is_null() && !(data.at(name).at("Warning").get<std::string>().empty()))
		return data.at(name).at("Warning").get<std::string>();
	else if (!(data["Warning"].is_null()))
		return data.at("Warning").get<std::string>();
	else
		return parentData->getWarning(this->path);
}

std::string DirFile::getSource(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Source").is_null() && !(data.at(name).at("Source").get<std::string>().empty()))
		return data.at(name).at("Source").get<std::string>();
	else if (!(data["Source"].is_null()))
		return data.at("Source").get<std::string>();
	else
		return parentData->getSource(this->path);
}

std::string DirFile::getComment(std::string path)
{
	std::string name = getFileNameFromPath(path);
	if (!this)
		return "";
	else if (!(data[name].is_null()) && !data.at(name).at("Comment").is_null() && !(data.at(name).at("Comment").get<std::string>().empty()))
		return data.at(name).at("Comment").get<std::string>();
	else if (!(data["Comment"].is_null()))
		return data.at("Comment").get<std::string>();
	else
		return parentData->getComment(this->path);
}

void DirFile::addJsonObj(nlohmann::json& json, std::string name)
{
	data[name] = json;
}

nlohmann::json& DirFile::getJsonObj(std::string name)
{
	return data[name];
}

void DirFile::read()
{
	if (getFileTerminator(path) == CONFIG_TERMINATOR)
	{
		std::ifstream f(path);
		f >> data;
	}
	if (data.size() == 0)
	{
		for (uint32_t i = 0; i < 10; i++)
			data[PNG::getKey(i)] = "";
	}
}

void DirFile::write()
{
	if (data.size() > 0)
	{
		std::ofstream o(path);
		o << std::setw(4) << data << std::endl;
	}
}

void DirFile::writeImages(std::string s, uint32_t offset)
{
	std::string p = path;
	p.replace(p.begin(), p.begin() + offset, s.begin(), s.end());
	if (!boost::filesystem::exists(getPath(p)))
		boost::filesystem::create_directory(getPath(p));
	for (File* f : children)
	{
		if (f->getType() == DIR)
			((DirFile*)f)->writeImages(s, offset);
		else if (f->getType() == PNG)
			((ImageFile*)f)->write(s, offset);
	}
}
void DirFile::readImages()
{
	for (File* f : children)
	{
		if (f->getType() == DIR)
			((DirFile*)f)->readImages();
		else if (f->getType() == PNG)
			((ImageFile*)f)->read();
	}
}

bool DirFile::addChild(std::string path)
{
	bool found = false;
	std::string p1 = getPath(this->path);
	std::string p2 = getPath(path);
	size_t s = p2.find(p1);
	if (s != -1)
	{
		for (File* child : children)
			if (child->addChild(path))
			{
				found = true;
				break;
			}
		if (found)
			return true;
		else
		{
			if (getFileTerminator(path) == CONFIG_TERMINATOR)
				children.push_back(new DirFile(path, this));
			else if (getFileTerminator(path) == IMAGE_TERMINATOR)
				children.push_back(new ImageFile(path, this));
			return true;
		}
	}
	else
		return false;
}

File::FileType DirFile::getType() { return DIR; }

DirFile::~DirFile()
{
	write();
	for (File* f : children)
	{

		if (f->getType() == DIR)
		{
			DirFile* file = ((DirFile*)f);
			file->~DirFile();
			free(file);
		}
		else if (f->getType() == PNG)
		{
			ImageFile* file = ((ImageFile*)f);
			file->~ImageFile();
			free(file);
		}
	}
}
