#include <string>


#pragma once
class PNG
{
public:
	PNG(std::string);

	void load();

	void write(std::string str);

	std::string getPath() { return path; }
	std::string getValue(const char*);
	static std::string getKey(uint32_t i);
	std::string getValue(uint32_t i);
	void setValue(const char*, const char*);

	~PNG();
private:
	std::string path;

	struct
	{
		std::string title;
		std::string author;
		std::string description;
		std::string copyright;
		std::string creation_time;
		std::string software;
		std::string disclaimer;
		std::string warning;
		std::string source;
		std::string comment;
	} textData;
	void addTextData(const char*, const char*);
	uint32_t getKeyID(const char*);

	uint8_t* _data;
	uint8_t* _dataWriteEnd;
	uint8_t* _dataEnd;

	uint8_t endChunk[12];

	void realloc(size_t newSize);
	void append(void* data, size_t size);
};

