#include <string>

#ifndef UTIL
	#define IMAGE_TERMINATOR ".png"
	#define CONFIG_TERMINATOR ".json"

	std::string getFileNameFromPath(std::string path)
	{
		uint32_t sep1 = path.find_last_of('\\');
		uint32_t sep2 = path.find_last_of('.');
		return path.substr( sep1 + 1, sep2 - sep1 - 1);
	}

	std::string getFileNameAndTerminatorFromPath(std::string path)
	{
		uint32_t sep = path.find_last_of('\\');
		return path.substr(sep + 1);
	}

	std::string getFileTerminator(std::string path)
	{
		uint32_t sep = path.find_last_of('.');
		return path.substr(sep);
	}

	std::string getPath(std::string path)
	{
		std::string p = path;
		uint32_t sep = path.find_last_of('\\');
		return std::string( p.begin(), p.begin() + sep);
	}

#define UTIL
#endif