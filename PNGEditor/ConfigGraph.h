#include "PNG.h"

#include <fstream>
#include <iomanip>
#include <json.hpp>

#pragma once

class DirFile;

class File
{
public:
	enum FileType
	{
		PNG,
		DIR
	};
	virtual bool addChild(std::string) { return false; }
	virtual FileType getType() = 0;
protected:
	std::string path;
	DirFile* parentData;
};

class ImageFile : public File
{
public:
	ImageFile(std::string path, DirFile* parentData);

	virtual FileType getType();

	void read();

	void write(std::string prefix, uint32_t offset);

private:
	::PNG png;
};

class DirFile : public File
{
public:
	DirFile(std::string path, DirFile* parentData = nullptr);

	std::string getTitle(std::string path);

	std::string getAuthor(std::string path);

	std::string getDescription(std::string path);

	std::string getCopyright(std::string path);

	std::string getCreationTime(std::string path);

	std::string getSoftware(std::string path);

	std::string getDisclaimer(std::string path);

	std::string getWarning(std::string path);

	std::string getSource(std::string path);

	std::string getComment(std::string path);

	void addJsonObj(nlohmann::json& json, std::string name);

	nlohmann::json& getJsonObj(std::string name);

	void read();

	void write();

	void writeImages(std::string s, uint32_t offset);
	void readImages();

	virtual bool addChild(std::string path);

	virtual FileType getType();

	~DirFile();

private:
	nlohmann::json data;
	std::vector<File*> children;
};
