#include "PNG.h"
#include "ConfigGraph.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <queue>
#include <boost\filesystem.hpp>


#define IMAGE_TERMINATOR ".png"
#define CONFIG_TERMINATOR ".json"

enum
{
	SCAN,
	READ,
	WRITE
} taskState;


void printPictureData(PNG& png, uint32_t i)
{
	std::string key = png.getKey(i);
	if (png.getValue(i) != "")
		std::cout << "\t" << key << ": " << png.getValue(i) << std::endl;
}

void printPictureData(PNG& png)
{
	size_t nameStart = png.getPath().find_last_of('\\');
	if (nameStart == -1)
		png.getPath().find_last_of('/');
	std::cout << "Reading Picture: " << png.getPath().substr(nameStart + 1) << std::endl;
	for (uint32_t i = 0; i < 10; i++)
		printPictureData(png, i);
}

void printHelp()
{
	std::cout << "Invalid Argument." << std::endl <<
		"Valid Options:" << std::endl <<
		"\tscan [path]\t\tScans for valid images and configs in path." << std::endl <<
		"\tread [path]\t\tGenerates config from images in path." << std::endl <<
		"\twrite [path]\t\tWrites configured data in images." << std::endl;
}

void scanPath(std::string str, std::queue<std::string>& dirs, std::vector<std::string>& images, std::vector<std::string>& config)
{
	auto it = boost::filesystem::directory_iterator(str);

	while (it != boost::filesystem::directory_iterator())
	{
		if (boost::filesystem::is_regular_file(it->path()))
		{
			std::string p = it->path().string();
			uint32_t term = p.find_last_of('.');
			if (term != -1)
			{
				p = p.substr(term);
				if (p == IMAGE_TERMINATOR)
					images.push_back(it->path().string());
				else if (p == CONFIG_TERMINATOR)
					config.push_back(it->path().string());
			}
		}
		else if (boost::filesystem::is_directory(it->path()))
		{
			dirs.push(it->path().string());
		}
		it++;
	}
}

int main(int argc, char* argv[])
{
	//Detecting current Path
	std::string currPath = std::string(argv[0]);
	currPath = currPath.substr(0, currPath.find_last_of('\\'));
	std::string tgtStr = "";

	std::cout << "Parsing Parameters." << std::endl;

	if (argc < 2)
	{
		printHelp();
		return 0;
	}
	else if (strcmp(argv[1], "scan") == 0)
	{
		taskState = SCAN;
	}
	else if (strcmp(argv[1], "read") == 0)
	{
		taskState = READ;
	}
	else if(strcmp(argv[1], "write") == 0)
	{
		taskState = WRITE;
	}
	else
	{
		printHelp();
		return 0;
	}

	std::cout << "Parsing Target Path." << std::endl;

	for (uint32_t i = 2; i < argc; i++)
	{
		tgtStr += argv[i];
		tgtStr += " ";
	}

	if (tgtStr.empty())
		tgtStr = currPath;
	else
		tgtStr.pop_back();


	std::cout << "Recursively scanning Directories." << std::endl;

	std::queue<std::string> paths;
	paths.push(tgtStr);

	std::vector<std::string> dirs;
	std::vector<std::string> images;
	std::vector<std::string> configs;

	std::cout << "Scanning Directories. " << dirs.size() << " left to go.";

	while (!paths.empty())
	{
		std::cout << "\rScanning Directories. " << dirs.size() << " left to go.";
		try
		{
			scanPath(paths.front(), paths, images, configs);
		}
		catch (boost::filesystem::filesystem_error& e)
		{
			std::cout << e.what() << std::endl;
			paths.pop();
			continue;
		}
		dirs.push_back(paths.front());
		paths.pop();
	}

	std::cout << std::endl;
	std::cout << "Scanned " << dirs.size() << " Directories." << std::endl;

	if (taskState == SCAN)
	{
		std::cout << "Detected Images:" << std::endl;
		for (std::string& s : images)
			std::cout << '\t' << s << std::endl;
		std::cout << "Detected Configs:" << std::endl;
		for (std::string& s : configs)
			std::cout << '\t' << s << std::endl;
		return 0;
	}

	if (configs.empty() && taskState == READ)
	{
		std::cout << "Generating config Files." << std::endl;
		for (uint32_t i = 0; i < dirs.size(); i++)
		{
			for (uint32_t j = 0; j < images.size(); j++)
			{
				if (images[j].find(dirs[i]) == 0)
				{
					std::ofstream emptyJson(dirs[i] + "\\config.json");
					emptyJson.write("{}", 2);
					emptyJson.close();
					configs.push_back(dirs[i] + "\\config.json");
				}
			}
		}
		std::cout << "Deduplicating found nodes." << std::endl;
		std::sort(configs.begin(), configs.end());
		configs.erase(std::unique(configs.begin(), configs.end()), configs.end());
	}


	std::cout << "Loading Configs: 0/" << configs.size();

	DirFile root = DirFile(configs[0]);
	for (uint32_t i = 1; i < configs.size(); i++)
	{
		root.addChild(configs[i]);
		std::cout << "\rLoading Configs: " << i << "/" << configs.size();
	}

	std::cout << "\rLoading Configs: " << configs.size() << "/" << configs.size() << std::endl;

	std::cout << "Loading Images: 0/" << images.size();

	for (uint32_t i = 0; i < images.size(); i++)
	{
		std::cout << "\rLoading Images: " << i << "/" << images.size();
		root.addChild(images[i]);
	}

	std::cout << "\rLoading Images: " << images.size() << "/" << images.size() << std::endl;


	if (taskState == WRITE)
	{
		std::cout << "Writing Files." << std::endl;
		root.writeImages(currPath + "\\modified\\", tgtStr.size() + 1);
	}
	else if (taskState == READ)
	{
		std::cout << "Reading Files." << std::endl;
		root.readImages();
		//compress Data
	}
	return 0;
}
