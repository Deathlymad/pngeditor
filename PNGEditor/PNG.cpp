#include "PNG.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <zlib.h>


PNG::PNG(std::string path) : path(path)
{
	_data = nullptr;
	_dataWriteEnd = nullptr;
	_dataEnd = nullptr;
}


PNG::~PNG()
{
}

void PNG::load()
{
	std::ifstream file(path, std::ios::in | std::ios::binary);

	file.seekg(0);

	uint8_t magicNbr[8];
	file.read((char*)&magicNbr, 8);

	if (magicNbr[0] != 0x89 ||
		magicNbr[1] != 0x50 ||
		magicNbr[2] != 0x4E ||
		magicNbr[3] != 0x47 ||
		magicNbr[4] != 0x0D ||
		magicNbr[5] != 0x0A ||
		magicNbr[6] != 0x1A ||
		magicNbr[7] != 0x0A)
	{
		std::cout << "Invalid magic number." << std::endl;
		file.close();
		return;
	}

	file.seekg(8);
	while (true)
	{
		uint8_t cLength[4];
		uint32_t length = 0;
		char type[4];
		uint32_t crc;
		file.read((char*)cLength, 4);
		length = cLength[0] << 24 | cLength[1] << 16 | cLength[2] << 8 | cLength[3];
		file.read(type, 4);
		char* data = (char*)malloc(length);
		file.read(data, length);
		file.read((char*)&crc, 4);

		
		if (type[0] == 'I' && type[1] == 'E' && type[2] == 'N' && type[3] == 'D')
		{
			memcpy(endChunk, &length, 4);
			memcpy(&endChunk[4], type, 4);
			memcpy(&endChunk[8], &crc, 4);
			free(data);
			break;
		}
		else if (type[0] == 'i' && type[1] == 'T' && type[2] == 'X' && type[3] == 't')
		{
			std::cout << "WARNING!!! iTXt chunk found. Unicode is not supported. Ignoring." << std::endl;
			//deleted to not invalidate rest of the data written with this;
		}
		else if (type[0] == 't' && type[1] == 'E' && type[2] == 'X' && type[3] == 't')
		{
			size_t sep = std::find(data, &data[length - 1], '\0') - data;
			if (sep == length - 1)
			{
				std::cout << "String empty or invalid tEXt chunk." << std::endl;
				continue;
			}

			char* val = (char*)malloc(length - sep);
			memcpy(val, &data[sep + 1], length - sep - 1);
			val[length - sep - 1] = '\0'; //null terminator

			addTextData(data, val);
			free(val);
		}
		else if (type[0] == 'z' && type[1] == 'T' && type[2] == 'X' && type[3] == 't')
		{
			size_t sep = std::find(data, &data[length - 1], '\0') - data;
			if (sep == length - 1)
			{
				std::cout << "String empty or invalid tEXt chunk." << std::endl;
				continue;
			}

			if (data[sep + 1] == 0)
			{
				std::vector<char> uncompressedData;
				int err;
				z_stream d_stream; /* decompression stream */

				d_stream.zalloc = Z_NULL;
				d_stream.zfree = Z_NULL;
				d_stream.opaque = Z_NULL;
				d_stream.avail_in = 0;
				d_stream.next_in = Z_NULL;
				err = inflateInit(&d_stream);
				if (err != Z_OK) {
					std::cerr << "inflateInit" << " error: " << err << std::endl;
					continue;
				}

				d_stream.avail_in = length - sep - 1;
				d_stream.next_in = (uint8_t*)&data[sep + 2];

				for (;;) {
					uint8_t d_buffer[10] = {};
					d_stream.next_out = &d_buffer[0];
					d_stream.avail_out = 10;

					err = inflate(&d_stream, Z_NO_FLUSH);

					if (err == Z_STREAM_END) {
						for (uint32_t i = 0; i < (10 - d_stream.avail_out); i++)
							uncompressedData.push_back(d_buffer[i]);
						if (d_stream.avail_in == 0)
							break;
					}

					if (err != Z_OK) {
						std::cerr << "inflate" << " error: " << err << std::endl;
						continue;
					}
					for (uint32_t i = 0; i < (10 - d_stream.avail_out); i++)
						uncompressedData.push_back(d_buffer[i]);
				}
				err = inflateEnd(&d_stream);
				if (err != Z_OK) {
					std::cerr << "inflateEnd" << " error: " << err << std::endl;
					continue;
				}

				addTextData(std::string(data, sep).c_str(), std::string(uncompressedData.begin(), uncompressedData.end()).c_str());
			}
		}
		else
		{
			append(cLength, 4);
			append(type, 4);
			append(data, length);
			free(data);
			append(&crc, 4);
		}
	}

	file.close();
}

void PNG::write(std::string str)
{
	std::ofstream file = std::ofstream(str, std::ios::out | std::ios::binary);

	const static uint8_t mgcNbr[8] = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A };
	file.write((char*)mgcNbr, 8);

	file.write((char*)_data, _dataEnd - _data);

	const static char type[] = { 't', 'E', 'X', 't'};

	for (uint32_t i = 0; i < 10; i++)
	{
		std::string key = getKey(i);
		std::string val = getValue(i);

		uint32_t len = key.size() + 1 + val.size();
		char cLen[] = { (char)(len >> 24), (char)((len >> 16) & 255), (char)((len >> 8) & 255), (char)(len & 255)};

		uint8_t* data = (uint8_t*)malloc(4 + len);
		memcpy(data, type, 4);

		if (!(key.empty() || val.empty()))
		{
			memcpy(&data[4], key.data(), key.size());
			data[4 + key.size()] = '\0';
			memcpy(&data[key.size() + 5], val.data(), val.size());
		}
		else
		{
			free(data);
			continue;
		}

		uLong crc = crc32(0, data, len + 5);
		char crc_c[4];
		memcpy(crc_c, &crc, 4);

		file.write(cLen, 4);
		file.write((char*)data, 4 + len);
		file.write(crc_c, 4);
		free(data);
	}

	file.write((char*)endChunk, 12);

	file.close();
}

std::string PNG::getValue(const char* key)
{
	return getValue(getKeyID(key));
}

void PNG::setValue(const char* key, const char* value)
{
	if (strcmp(value, ""))
		addTextData(key, value);
}

void PNG::addTextData(const char* key,const char* value)
{
	if (strcmp("Title", key) == 0) {
		textData.title = value;
	}
	else if (strcmp("Author", key) == 0) {
		textData.author = value;
	}
	else if (strcmp("Description", key) == 0) {
		textData.description = value;
	}
	else if (strcmp("Copyright", key) == 0) {
		textData.copyright = value;
	}
	else if (strcmp("Creation Time", key) == 0) {
		textData.creation_time = value;
	}
	else if (strcmp("Software", key) == 0) {
		textData.software = value;
	}
	else if (strcmp("Disclaimer", key) == 0) {
		textData.disclaimer = value;
	}
	else if (strcmp("Warning", key) == 0) {
		textData.warning = value;
	}
	else if (strcmp("Source", key) == 0) {
		textData.source = value;
	}
	else if (strcmp("Comment", key) == 0) {
		textData.comment = value;
	}
}

uint32_t PNG::getKeyID(const char* key)
{
	if (strcmp("Title", key) == 0) {
		return 0;
	}
	else if (strcmp("Author", key) == 0) {
		return 1;
	}
	else if (strcmp("Description", key) == 0) {
		return 2;
	}
	else if (strcmp("Copyright", key) == 0) {
		return 3;
	}
	else if (strcmp("Creation Time", key) == 0) {
		return 4;
	}
	else if (strcmp("Software", key) == 0) {
		return 5;
	}
	else if (strcmp("Disclaimer", key) == 0) {
		return 6;
	}
	else if (strcmp("Warning", key) == 0) {
		return 7;
	}
	else if (strcmp("Source", key) == 0) {
		return 8;
	}
	else if (strcmp("Comment", key) == 0) {
		return 9;
	}
	return 10;
}

std::string PNG::getValue(uint32_t i)
{
	switch (i)
	{
	case 0:
		return textData.title;
	case 1:
		return textData.author;
	case 2:
		return textData.description;
	case 3:
		return textData.copyright;
	case 4:
		return textData.creation_time;
	case 5:
		return textData.software;
	case 6:
		return textData.disclaimer;
	case 7:
		return textData.warning;
	case 8:
		return textData.source;
	case 9:
		return textData.comment;
	default:
		return "";
	}
}

std::string PNG::getKey(uint32_t i)
{
	switch (i)
	{
	case 0:
		return "Title";
	case 1:
		return "Author";
	case 2:
		return "Description";
	case 3:
		return "Copyright";
	case 4:
		return "Creation Time";
	case 5:
		return "Software";
	case 6:
		return "Disclaimer";
	case 7:
		return "Warning";
	case 8:
		return "Source";
	case 9:
		return "Comment";
	default:
		return "";
	}
}

void PNG::realloc(size_t newSize)
{
	size_t writeOffset = _dataWriteEnd - _data;
	uint8_t* newData = (uint8_t*)malloc(newSize);
	if (newSize < writeOffset)
		writeOffset = newSize;
	memcpy(newData, _data, writeOffset);
	free(_data);
	_data = newData;
	_dataWriteEnd = newData + writeOffset;
	_dataEnd = newData + newSize;
}

void PNG::append(void * data, size_t size)
{
	if (size >= (size_t)(_dataEnd - _dataWriteEnd))
	{
		realloc((_dataEnd - _data) + (size - (_dataEnd - _dataWriteEnd)));
	}

	memcpy(_dataWriteEnd, data, size);
	_dataWriteEnd += size;
}
